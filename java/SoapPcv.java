package com.EDUCAUSE.PingFederate.PCV;

import java.util.Collections;
//import com.EDUCAUSE.PingFederate.Common.GuiHelper;
//import com.EDUCAUSE.PingFederate.Common.SoapHelper;
//import com.EDUCAUSE.PingFederate.Common.SoapSharedConstants;
import com.pingidentity.sdk.GuiConfigDescriptor;
import com.pingidentity.sdk.PluginDescriptor;
import com.pingidentity.sdk.password.PasswordCredentialValidator;
import com.pingidentity.sdk.password.PasswordValidationException;


import org.sourceid.util.log.AttributeMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sourceid.saml20.adapter.conf.Configuration;
import org.sourceid.saml20.adapter.gui.*;
import org.sourceid.saml20.adapter.gui.validation.impl.HttpURLValidator;
import org.sourceid.saml20.adapter.gui.validation.impl.RequiredFieldValidator;
import org.sourceid.saml20.adapter.attribute.AttributeValue;
import org.sourceid.saml20.adapter.conf.*;
import org.sourceid.util.log.*;
import com.pingidentity.sdk.*;
import com.pingidentity.sdk.password.*;


//import org.sourceid.saml20.adapter.conf.Row;
//import org.sourceid.saml20.adapter.conf.Table;
import org.sourceid.saml20.adapter.gui.validation.impl.RequiredFieldValidator;
import org.sourceid.util.log.AttributeMap;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Properties;


import javax.annotation.Resource;
import javax.xml.soap.*;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.transform.*;

import javax.xml.transform.stream.*;

import java.awt.List;
import java.security.Security;
import java.util.Iterator;
public class SoapPcv implements PasswordCredentialValidator {
	
	
	
    /** Define the logger object common to PingFederate for server.log */
    private final Log log = LogFactory.getLog(this.getClass());

    //private GuiHelper guiHelper = new GuiHelper();

    /** Define the UX interface and metadata descriptor for the Admin Console */
    //private PluginDescriptor descriptor = null;
    
    
    //Add DataStore
    //String SoapDataStore = null;
    
    private static String USERNAME = "Username";
    private static String PASSWORD = "Password";
    private  static String USERNAME_ATTRIBUTE = "username";
    private static String TYPE = "Educause PVC";
    private static String URL = "API_ENDPOINT";
    
    
    //private Map<String, String> userNamePasswordMap;
    

    //Configuration configuration;

    String userName = "";
    String password = "";
    
    String url = "";

    
//This method takes a username and password and verifies the credential against an external source. If the credentials are valid, then an AttributeMap is returned containing at least one entry representing the principal. 
//If the credentials are invalid, then null or an empty map is returned. 
//A PasswordValidationException is thrown if the plug-in was unable to validate the credentials (for example, due to an offline host or network problems
    @Override
    public AttributeMap processPasswordCredential(String userName, String password) throws PasswordValidationException {
    	
    	

        // 
        AttributeMap attributeMap = null;
        
        /*log.debug("processPasswordCredential(): BEGIN");

        if (StringUtils.isBlank(userName)) {
            return null;
        }
        else if(StringUtils.isBlank(password)) {
            return null;
        } else {*/
        	
        	
        if (userName == null && password == null)
        {
            throw new PasswordValidationException("Invalid username and/or password");
        }

        if (this.userName.equalsIgnoreCase(userName) && this.password.equals(password))
        {
            attributeMap = new AttributeMap();
            attributeMap.put(USERNAME_ATTRIBUTE, new AttributeValue(userName));
            
            try{
            	SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
                SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            log.debug("Start Request");
            
    		//String url = "http://edu.netforument.com/nfedutest/xweb/secure/netForumXML.asmx";
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(userName,password), url);
            
            log.debug("Response Brought Back");
            
            log.debug("soapResponse before token: " + soapResponse);
            
            // Process the SOAP Response
             getAccessToken(soapResponse);
            
             log.debug("Token Received");
             
             log.debug("soapResponse after token: " + soapResponse);
            soapConnection.close();
    		
            }catch(Exception e) {
                log.debug("Error occurred while sending SOAP Request to Server");
              
            }
    		
        }
        else
        {
            // authentication failed return null or an empty map
            return null;
        }

        return attributeMap;
    }
    
        	
    
        public String getAccessToken(SOAPMessage soapResponse)throws Exception{
        	
        	//TransformerFactory transformerFactory = TransformerFactory.newInstance();
            //Transformer transformer = transformerFactory.newTransformer();
        	
        	//Source sourceContent = soapResponse.getSOAPPart().getContent();
            log.debug("Access Token" + soapResponse.getSOAPHeader().getTextContent());
            return soapResponse.getSOAPHeader().getTextContent();
        }
        
       
        
        
     
        
        

        

        
        
    
        
        
        
        
    










        private  SOAPMessage createSOAPRequest(String userName, String password) throws Exception {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();
           
            
            String serverURI = "http://www.avectra.com/2005/";

            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration("ns", serverURI);

            /*
            Constructed SOAP Request Message:
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://ws.cdyne.com/">
                <SOAP-ENV:Header/>
                <SOAP-ENV:Body>
                    <example:VerifyEmail>
                        <example:email>mutantninja@gmail.com</example:email>
                        <example:LicenseKey>123</example:LicenseKey>
                    </example:VerifyEmail>
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>
             */
            log.debug("Header start");
            
            MimeHeaders headers = soapMessage.getMimeHeaders();
           log.debug("Soap Action Start");
            headers.addHeader("SOAPAction", serverURI  + "Authenticate");
            log.debug("Soap Action finish");
            // SOAP Body
            log.debug("Body Start");
            SOAPBody soapBody = envelope.getBody();
          
            SOAPElement soapBodyElem = soapBody.addChildElement("Authenticate", "ns");
            SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("userName", "ns");
            soapBodyElem1.addTextNode(userName);
            SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("password", "ns");
            soapBodyElem2.addTextNode(password);
            log.debug("Body Finish");
            

            soapMessage.saveChanges();
            
            /* Print the request message 
            System.out.print("Request SOAP Message = ");
            soapMessage.writeTo(System.out);
            System.out.println();*/
            log.debug("Message saved and returning...");
            return soapMessage;
        }
        
        
        
           //Get key by authenticating with service account username and service account password against url

/*SoapHelper helper = new SoapHelper();
        	try {
				//String accessToken = helper.getAccessToken(userName,password,url);
			} catch (SOAPException e) {
				// TODO Auto-generated catch block
				log.error("error getting access token", e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error("error getting access token", e);
			}
            //Get any additional attributes configured in the PCV
            //Object attrsNames = this.configuration.getAdditionalAttrNames();
            //if (((Set) attrsNames).isEmpty()) {
              //  attrsNames = new HashSet();
            }*/

        
             
            
        
        
        //return null;
    

    @Override
    public void configure(Configuration configuration) {
        //this.configuration = configuration;
    	this.userName = configuration.getFieldValue(USERNAME);
        this.password = configuration.getFieldValue(PASSWORD);
        //Table table = configuration.getTable(SoapSharedConstants.TABLE_NAME_USERS);
        //this.userNamePasswordMap = new HashMap();
        
        //userName = configuration.getFieldValue(SoapSharedConstants.SERVICE_ACCOUNT_USERNAME_NAME);
       // password = configuration.getFieldValue(SoapSharedConstants.SERVICE_ACCOUNT_PASSWORD_NAME);
        log.debug("Here is the start of URL");
          this.url = configuration.getFieldValue(URL);
        
        //this.SoapDataStore = configuration.getFieldValue(SoapSharedConstants.SOAP_DATASOURCE);
        
        //this.userNamePasswordMap.put(userName, password);
        
        //Log Method exit
        this.log.debug("configure(); END");
        
        
    }

    /**
     * The PingFederate server will invoke this method on the Soap PCV to discover
     * metadata about the implementation. This includes how PingFederate will render within the
     * Admin Console for configuration of the Soap PCV, and associated metadata
     * returned in the hashmap to the PingFederate server at runtime when the PCV is invoked.
     * <br/>
     * This implementation always returns the same PluginDescriptor object from call to call -
     * behaviour of the system is undefined if this convention is not followed.
     *
     * @return A {@link PluginDescriptor} that describes this plugin to the PingFederate server.
     */
    //start point to create menu in console. Goes to createPluginDescriptor to finish
    @Override
    public PluginDescriptor getPluginDescriptor() {
        // Log so can see IdP Adapter is appropriately initialized
        log.debug("getPluginDescriptor(): BEGIN");
        RequiredFieldValidator requiredFieldValidator = new RequiredFieldValidator();

        GuiConfigDescriptor guiDescriptor = new GuiConfigDescriptor();
        guiDescriptor.setDescription(TYPE);

        TextFieldDescriptor usernameFieldDescriptor = new TextFieldDescriptor(USERNAME, USERNAME);
        usernameFieldDescriptor.addValidator(requiredFieldValidator);
        guiDescriptor.addField(usernameFieldDescriptor);

        TextFieldDescriptor passwordFieldDescriptor = new TextFieldDescriptor(PASSWORD, PASSWORD, true);
        passwordFieldDescriptor.addValidator(requiredFieldValidator);
        guiDescriptor.addField(passwordFieldDescriptor);
        
        TextFieldDescriptor urlFieldDescriptor = new TextFieldDescriptor(URL, URL, true);
        urlFieldDescriptor.addValidator(requiredFieldValidator);
        
        guiDescriptor.addField(urlFieldDescriptor);

        PluginDescriptor pluginDescriptor = new PluginDescriptor(TYPE, this, guiDescriptor);
        pluginDescriptor.setAttributeContractSet(Collections.singleton(USERNAME_ATTRIBUTE));
        pluginDescriptor.setSupportsExtendedContract(false);
        return pluginDescriptor;
        /*if (this.descriptor == null)
        {
            this.descriptor = this.createPluginDescriptor();
        }*/
        //log.debug("getPluginDescriptor(): END");

        //return (this.descriptor);
    }
    
    
    
}



    /**
     * This method will create and populate the PluginDescriptor with objects (e.g. controls)
     * that appear in the PingFederate administration console when the admin is creating/updating an
     * instance of the PEGRight Simple PCV.  The metadata for the PCV defines the attribute contract
     * that the adapter returns to the PingFederate Server at runtime when the PCV is executed.
     *
     * @return an PluginDescriptor object that describes this PEGRight PCV implementation.
     */
    //Actual creation of menu in server
   /* private PluginDescriptor createPluginDescriptor()
    {
        // Log so can see Plugin Descriptor is created
        this.log.debug("createPluginDescriptor(): BEGIN");

        // Create the Admin GUI Descriptor for the Plugin
        // Title of PCV
        GuiConfigDescriptor guiConfigDesc = new GuiConfigDescriptor(SoapPcvConstants.COMPONENT_DESC);

        guiHelper.addTextField(guiConfigDesc, SoapSharedConstants.SERVICE_ACCOUNT_USERNAME_NAME, SoapSharedConstants.SERVICE_ACCOUNT_USERNAME_DESC, "", true, false, 100);
        guiHelper.addTextField(guiConfigDesc, SoapSharedConstants.SERVICE_ACCOUNT_PASSWORD_NAME, SoapSharedConstants.SERVICE_ACCOUNT_PASSWORD_DESC, "", true, true, 100);
        
        //commented out to get workable PVC
        //guiHelper.addURLField(guiConfigDesc, SoapSharedConstants.API_ENDPOINT_URL_NAME, SoapSharedConstants.API_ENDPOINT_URL_DESC, "", true, 100);

        // Create the plugin descriptor ... includes UX and metadata attribute contract
        this.descriptor = new PluginDescriptor(SoapPcvConstants.COMPONENT_NAME, this, guiConfigDesc, SoapPcvConstants.PLUGIN_VERSION);
        Set<String> attributes = new HashSet();
        attributes.add(SoapPcvConstants.ATTR_NAME_USERNAME);

        this.descriptor.setAttributeContractSet(attributes);
        this.descriptor.setSupportsExtendedContract(true);

        // Log the exit for debugging
        this.log.debug("createPluginDescriptor(): END");

        return descriptor;
    }
}*/
